<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('probs', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('level');
	        $table->string('user_id');
	        $table->string('title');
	        $table->string('ict_staff')->nullable();
	        $table->text('body')->nullable();
	        $table->text('comment')->nullable();
	        $table->string('staff_title')->nullable();
	        $table->string('status')->default('Pending');
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('probs');
    }
}
