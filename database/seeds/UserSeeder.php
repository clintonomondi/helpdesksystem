<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('users')->insert([
		    'name' => 'Queen Admin',
		    'email' => 'admin'.'@gmail.com',
		    'user_type'=>'admin',
		    'dep_id'=>'1',
		    'level'=>'admin',
		    'password' => bcrypt('admin'),
	    ]);
    }
}
