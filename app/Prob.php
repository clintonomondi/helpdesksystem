<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prob extends Model
{
	protected  $fillable=['level','user_id','title','body','status','comment','ict_staff','staff_title'];

	public  function user(){
		return $this->belongsTo(User::class);
	}
}
