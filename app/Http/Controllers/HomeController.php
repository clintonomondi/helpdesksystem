<?php

namespace App\Http\Controllers;

use App\Dep;
use App\Prob;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	$pending=Prob::where('status','Pending')->count();
    	$sorted=Prob::where('status','Sorted')->count();
    	$users=User::count();
    	$deps=Dep::all();

    	$ictpending=Prob::where('status','Pending')->where('level',Auth::user()->level)->count();
    	$ictsorted=Prob::where('status','Sorted')->where('level',Auth::user()->level)->count();

    	$staffpending=Prob::where('status','Pending')->where('user_id',Auth::user()->id)->count();
    	$staffsorted=Prob::where('status','Sorted')->where('user_id',Auth::user()->id)->count();
        return view('home',compact('users','pending','sorted','deps','ictpending','ictsorted','staffpending','staffsorted'));
    }
    public  function profile(){
    	$user=User::find(Auth::user()->id);
    	$deps=Dep::all();
    	return view('profile',compact('user','deps'));
    }
	public  function updateprofile(Request $request){
		$request->validate([
			'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
		]);
		//get file name with extension
		$fileNameWithExt=$request->file('avatar')->getClientOriginalName();
		$filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
		$extesion=$request->file('avatar')->getClientOriginalExtension();
		//filename to store
		$fileNameToStore=$filename.'_'.time().'.'.$extesion;
		//uploadimage
		$path=$request->file('avatar')->storeAs('/public/avatars',$fileNameToStore);

		$user=User::find(Auth::user()->id);
		$user->avatar=$fileNameToStore;
		$user->save();
		return redirect()->back()->with('success','Profile pic updated successfully');
	}

	public  function password(Request $request){
		$this->validate($request, [
			'newpass' => 'required|min:5',
			'repass' => 'required|min:5',
		]);
		$currentpass = auth()->user()->password;
		if (!Hash::check($request['currentpass'], $currentpass)) {
			return redirect()->back()->with('error', 'Sorry the entered current password is wrong.');
		} elseif ($request->input('newpass') !== $request->input('repass')) {
			return redirect()->back()->with('error', 'Sorry the new passwords don\'t match.');
		}
		$currentUser = User::findOrFail(Auth::user()->id);
		$currentUser->password = Hash::make($request->input('newpass'));
		$currentUser->save();
		return redirect()->back()->with('success','Password changed successfully');
	}

	public  function updateaccount(Request $request){
		$name=$request->input('name');
		$email=$request->input('email');
		$dep_id=$request->input('dep_id');
		$user=User::findorFail(Auth::user()->id);
		$user->name=$name;
		$user->email=$email;
		$user->dep_id=$dep_id;
		$user->save();
		return redirect()->back()->with('success','User account updated successfully');
	}

	public  function myrequests(){
     $requests=User::find(Auth::user()->id)->prob()->orderBy('status','desc')->paginate(5);
     return view('myrequests',compact('requests'));
	}


	public  function postproblem(Request $request){
    	$pos=Prob::create($request->all());
		return redirect()->back()->with('success','Request submitted successfully successfully');

	}
	public  function  readmyreq($id){
     $prob=Prob::find($id);
     return view('readmyreq',compact('prob'));

	}
	public  function updateprob(Request $request){
		$id=$request->input('id');
		$tile=$request->input('title');
		$level=$request->input('level');
		$body=$request->input('body');
		$prob=Prob::findorFail($id);
		$prob->title=$tile;
		$prob->level=$level;
		$prob->body=$body;
		$prob->save();
		return redirect()->back()->with('success','Request updated successfully');
	}

	public  function cancel($id){
    	$post=Prob::find($id);
    	$post->delete();
    	return redirect()->route('myrequests')->with('success','Request cancelled successfully');
	}

	public  function  forward(Request $request){
    	$id=$request->input('id');
    	$level=$request->input('level');
    	$prob=Prob::find($id);
    	$prob->level=$level;
    	$prob->save();
		return redirect()->back()->with('success','Request forwarded successfully');
	}
	public  function solve(Request $request){
		$id=$request->input('id');
		$status=$request->input('status');
		$comment=$request->input('comment');
		$ict_staff=$request->input('ict_staff');
		$prob=Prob::find($id);
		$prob->comment=$comment;
		$prob->status=$status;
		$prob->ict_staff=$ict_staff;
		$prob->save();
		return redirect()->back()->with('success','Request commented successfully');
	}

	public  function updatedep(Request $request){
    	$dep_id=$request->input('dep_id');
    	$user=User::find(Auth::user()->id);
    	$user->dep_id=$dep_id;
    	$user->save();
		return redirect()->back()->with('success','Application submitted successsfully,kindly be patient as we approve your application');

	}


	public  function pending(){
		$probs=Prob::where('status','Pending')->where('level',Auth::user()->level)->orderBy('staff_title','asc')->paginate(5);
		return view('ict.pending',compact('probs'));
	}
	public function sorted(){
		$probs=Prob::where('status','Sorted')->orderBy('created_at','desc')->paginate(5);
		return view('ict.sorted',compact('probs'));
	}
}
