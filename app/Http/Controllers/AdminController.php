<?php

namespace App\Http\Controllers;

use App\Dep;
use App\Prob;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public  function users(){
 $users=User::all();
 return view('admin.users',compact('users'));
	}

	public  function departments(){
		$deps=Dep::all();
		return view('admin.deps',compact('deps'));
	}

	public  function postdep(Request $request){
		$this->validate($request, [
			'name' => 'required|unique:deps',
		]);
		$dep=Dep::create($request->all());
		return redirect()->back()->with('success','Department added successfully');
	}

	public  function removedep($id){
		$dep=Dep::find($id);
		$dep->delete();
		return redirect()->back()->with('success','Department removed successfully');
	}
	public  function editdep($id){
		$dep=Dep::find($id);
		return view('admin.editdep',compact('dep'));
	}

	public  function updatedep(Request $request){
		$id=$request->input('id');
		$name=$request->input('name');
		$dep=Dep::find($id);
		$dep->name=$name;
		$dep->save();
		return redirect()->back()->with('success','Department updated successfully');
	}

	public  function userprofile($id){
		$user=User::find($id);
		$deps=Dep::all();
		return view('admin.userprofile',compact('user','deps'));
	}

	public  function updaterole(Request $request){
		$id=$request->input('id');
		$user_type=$request->input('user_type');
		$level=$request->input('level');
		$title=$request->input('title');
		$user=User::find($id);
		$user->user_type=$user_type;
		$user->level=$level;
		$user->title=$title;
		$user->save();
		return redirect()->back()->with('success','User updated successfully');
	}

	public  function  all(){
		$probs=Prob::orderBy('created_at','desc')->paginate(5);
		return view('admin.all',compact('probs'));
	}
	public  function pending(){
		$probs=Prob::where('status','Pending')->orderBy('created_at','desc')->paginate(5);
		return view('admin.pending',compact('probs'));
	}
	public  function sorted(){
		$probs=Prob::where('status','Sorted')->orderBy('created_at','desc')->paginate(5);
		return view('admin.sorted',compact('probs'));
	}
	public  function read($id){
		$prob=Prob::find($id);
		return  view('admin.read',compact('prob'));

	}

	public  function removeuser($id){
		$user=User::find($id);
		$user->delete();
		return redirect()->back()->with('success','User removed successfully');
	}
}
