@extends('layouts.app')

@section('content')
	@include('includes.message')
	@include('modal.updaterequest')
	<div class="row justify-content-center">
		<div class="col-sm-6 col-sm-4">
			<div class="feature-block">
				<span class="fa fa-pull-left"> <h3>{{$prob->title}}</h3></span>
				<span class="fa fa-pull-right"> <h6>{{$prob->created_at->diffForHumans()}}</h6></span>
				<hr><br>
				<p>{{$prob->body}}</p>
				@if(($prob->status)=='Pending')
					<div class="progress">
						<div class="progress-bar progress-bar-striped progress-bar-animated" style="width:50%">Pending</div>
					</div>
					<hr>
					<div>
						<a class="btn btn-outline-success btn-sm fa fa-edit" data-toggle="modal" data-target="#updaterequest" >Update</a>
						<a class="btn btn-outline-danger btn-sm fa fa-cut" href="{{route('cancel',$prob->id)}}">Cancel</a>
						@elseif(($prob->status)=='Sorted')
							<div class="progress">
								<div class="progress-bar bg-success " style="width:100%">Sorted</div>
							</div>
							<span class="fa fa-pull-left"><h6>Solved by: {{$prob->ict_staff}}  {{$prob->updated_at->diffForHumans()}}</h6></span>
								@endif

							</div>
					</div>
			</div>
	</div>
@endsection
