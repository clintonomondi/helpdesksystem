@extends('layouts.app')

@section('content')
	<div class="container">
		@include('includes.message')
		@include('modal.password')
		@include('modal.profile')
	<div class="row justify-content-center">
			<div class="col-sm-6 col-sm-6">
				<table class="table">
					<tr>
						<td class="fa fa-envelope">Name</td>
						<td>{{$user->name}}</td>
					</tr>
					<tr>
						<td class="fa fa-sync">Email</td>
						<td >{{$user->email}}</td>
					</tr>
					<tr>
						<td class="fa fa-check">Department</td>
						<td>{{$user->dep->name}}</td>
					</tr>
					<tr>
						<td class="fa fa-trash">Level</td>
						<td>{{$user->level}}</td>
					</tr>
					<tr>
						<td class="fa fa-trash"> Role</td>
						@if(($user->user_type)=='admin')
							<td style="color: red;">{{$user->user_type}}</td>
						@else
							<td style="color: green;">{{$user->user_type}}</td>
						@endif
					</tr>
					<tr>
						<td><a class="btn btn-outline-success btn-sm fa fa-key" data-toggle="modal" data-target="#password">Change password</a> </td>
						<td><a class="btn btn-outline-primary btn-sm fa fa-edit" data-toggle="modal" data-target="#profile">Edit profile</a> </td>
					</tr>

					</tbody>
				</table>
			</div>
			<div class="col-sm-6 col-sm-6">
		<div class="profile-header-container">
			<div class="profile-header-img">
				<img class="rounded-circle" src="/storage/avatars/{{ $user->avatar }}" style="height: 200px;width: 200px;" />
				<!-- badge -->
				<div class="rank-label-container">
					<span class="label label-default rank-label">{{$user->name}}</span>
				</div>
			</div>
		</div>

	<div class="row justify-content-center">
		<form action="{{route('updateprofile')}}" method="post" enctype="multipart/form-data">
			@csrf
			<div class="form-group">
				<input type="file" class="form-control-file" name="avatar" id="avatarFile" aria-describedby="fileHelp">
				<small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
			</div>
			<button type="submit" class="btn btn-outline-success btn-sm">Save</button>
		</form>
	</div>
	</div>
	</div>
	</div>
@endsection