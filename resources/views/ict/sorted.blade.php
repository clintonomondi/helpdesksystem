@extends('layouts.app')
@section('content')
	<div class="container">
		<ul class="nav nav-tabs">
			<li class="nav-item">
				<a class="nav-link  fa fa-sync" href="{{route('ict.pending')}}">Pending requests</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active fa fa-check" style="color: green" href="{{route('ict.sorted')}}">Sorted requests</a>
			</li>
		</ul>
		<table class="table table-striped">
			<thead >
			<th>#</th>
			<th>Level</th>
			<th>Title</th>
			<th>Sender</th>
			<th>Sorted by</th>
			<th>Date</th>
			<th></th>
			<th>Status</th>
			<th></th>
			</thead>
			<tbody id="myTable">
			@if(count($probs)>0)
				@foreach($probs as $key => $prob )
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$prob->level}}</td>
						<td>{{$prob->title}}</td>
						<td>{{$prob->user->name}}</td>
						<td>{{$prob->ict_staff}}</td>
						<td>{{$prob->created_at}}</td>
						<td>{{$prob->created_at->diffForHumans()}}</td>
						@if(($prob->status)=='Pending')
							<td><a class="btn btn-primary btn-sm fa fa-sync" href="{{route('admin.read',$prob->id)}}">Pending</a> </td>
						@elseif(($prob->status)=='Sorted')
							<td><a class="btn btn-success btn-sm fa fa-check" href="{{route('admin.read',$prob->id)}}" >Solved</a> </td>
						@endif
					</tr>
				@endforeach
			@else
				<p>No  records</p>
			@endif
			</tbody>
		</table>
		{{$probs->links()}}
	</div>
@endsection
