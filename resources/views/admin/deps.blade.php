@extends('layouts.app')
@section('content')
			<div class="container">
				@include('includes.message')
				@include('modal.adddep')
				<div class="row justify-content-center">
					<div class="col-sm-8 col-sm-6">
						<div class="feature-block">
							<span class="fa fa-pull-left"><h3>Departments</h3></span>
							<span class="fa fa-pull-right"><a class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#adddep">Add Department</a> </span>
							<table class="table table-striped">
								<thead >
								<th>#</th>
								<th>Department Name</th>
								<th></th>
								<th></th>
								</thead>
								<tbody id="myTable">
								@if(count($deps)>0)
									@foreach($deps as $dep)
										<tr>
											<td>{{$dep->id}}</td>
											<td>{{$dep->name}}</td>
											<td><a class="fa fa-edit" href="{{route('editdep',$dep->id)}}" style="color: blue">Edit</a> </td>
											{{--<td><a class="fa fa-cut" href="{{route('removedep',$dep->id)}}" style="color: red">Remove</a> </td>--}}
										</tr>
									@endforeach
								@else
									<p>No departments records</p>
								@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
@endsection
