@extends('layouts.app')

@section('content')
	Users
			@include('includes.message')
			<table class="table table-striped">
				<thead>
				<th>#</th>
				<th>Name</th>
				<th>Role</th>
				<th>Email</th>
				<th>Department</th>
				<th>Level</th>
				<th>Title</th>
				<th></th>
				<th></th>
				<th></th>
				</thead>
				<tbody id="myTable">
				@if(count($users)>0)
					@foreach($users as $user)
						<tr>
							<td>{{$user->id}}</td>
							<td>{{$user->name}}</td>
							@if(($user->user_type)=='admin')
								<td style="color: blue;">{{$user->user_type}}</td>
								@elseif(($user->user_type)=='NotAssigned')
								<td style="color: red;">{{$user->user_type}}</td>
							@else
								<td>{{$user->user_type}}</td>
							@endif
							<td>{{$user->email}}</td>
							<td>{{$user->dep->name}}</td>
							<td>{{$user->level}}</td>
							<td>{{$user->title}}</td>
							<td><img class="rounded-circle" src="/storage/avatars/{{ $user->avatar }}" style="height: 50px; width: 50px;" alt="no image" /></td>
							<td><a class="fa fa-user btn-primary btn-sm"  href="{{route('admin.userprofile',$user->id)}}">Profile</a> </td>
							<td><a class="fa fa-trash btn-danger btn-sm"  href="{{route('removeuser',$user->id)}}">Remove</a> </td>

						</tr>
					@endforeach
				@else
					<p>No No user yet</p>
				@endif
				</tbody>
			</table>
@endsection
