@extends('layouts.app')

@section('content')
	<div class="row justify-content-center">
		<div class="col-sm-4">
			@include('modal.changerole')
			@include('includes.message')
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">{{$user->name}}</h4>
					<table class="table">
						<tr>
							<td class="fa fa-bacon">Department</td>
							<td>{{$user->dep->name}}</td>
						</tr>
						<tr>
							<td class="fa fa-paint-brush">Level</td>
							<td>{{$user->level}}</td>
						</tr>
						<tr>
							<td class="fa fa-user"> User type</td>
							@if(($user->user_type)=='admin')
								<td style="color: red;">{{$user->user_type}}</td>
							@else
								<td style="color: green;">{{$user->user_type}}</td>
							@endif
						</tr>
						<tr>
							<td class="fa fa-umbrella">Title</td>
							<td>{{$user->title}}</td>
						</tr>

						</tbody>
					</table>
					<a href="#" class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#changeRole">Update</a>
				</div>
				<img class="card-img-top" src="/storage/avatars/{{ $user->avatar }}" alt="Profile image" style="width:100%; height: 200px;">
			</div>
			<br>
		</div>
	</div>
@endsection
