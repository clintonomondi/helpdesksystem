@extends('layouts.app')
@section('content')
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-sm-8 col-sm-6">
						<div class="feature-block">
							<span class="fa fa-pull-left"><h3>Department of {{$dep->name}}</h3></span>
							<span class="fa fa-pull-right"><a class="btn-outline-success btn-sm fa fa-backward" href="{{route('departments')}}">Deparments</a> </span>
							@include('includes.message')
							<form method="post" action="{{route('updatedep')}}">
								@csrf
								<div class="form-group">
									<label for="email">Department name:</label>
									<input type="text" class="form-control form-control-sm" id="email" name="name" value="{{$dep->name}}" required>
									<input type="text" class="form-control" id="email" name="id" value="{{$dep->id}}" hidden>
								</div>
								<div class="form-group form-check">
								</div>
								<button type="submit" class="btn btn-outline-success btn-sm">Update</button>
							</form>
						</div>
					</div>
				</div>
			</div>
@endsection
