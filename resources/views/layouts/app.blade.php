<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>HelpDesk</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{asset('css/fontawesome/css/all.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/style.js') }}" defer></script>
    <script src="{{asset('js/JQuery-3.3.1.min.js')}}"></script>

</head>
<body onload="openNav()">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-success navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{route('profile') }}">
                    @guest
                        @else
                    <img class="rounded-circle" src="/storage/avatars/{{ Auth::user()->avatar }}" style="width:50px;" alt="Upload image" />
                    @endguest
                    ICT HelpDesk
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <form class="form-inline">
                            <input class="form-control form-control-sm" type="text" placeholder="Search" id="myInput">
                        </form>
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
	                                @cannot('isNotAssigned')
                                    <a class="dropdown-item" href="{{route('profile')}}">
                                        Profile
                                    </a><hr>
	                                @endcannot
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @guest
            @else
			@cannot('isNotAssigned')
            <div id="mySidebar" class="sidebar border border-success">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
            <a class="fa  fa-home" href="{{route('home')}}">Home</a>
	            @can('isAdmin')
            <a class="fa fa-user" href="{{route('users')}}">Users</a>
            <a class="fa fa-bacon" href="{{route('departments')}}">Departments</a>
		            <a class="fa fa-reply" href="{{route('admin.all')}}">Admin Requests</a>
	            @endcan
            <a class="fa fa-hands" href="{{route('myrequests')}}">My Requests</a>
	            @can('isICT')
            <a class="fa fa-reply" href="{{route('ict.pending')}}">ICT Requests</a>
	            @endcan
        </div>
				@endcannot
        @endguest
        <div id="main">
            <button class="openbtn bg-success" onclick="openNav()">☰ Menu</button>
            <main class="py-4">
                @yield('content')
            </main>
        </div>

    </div>
    <script>
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
</body>
</html>
