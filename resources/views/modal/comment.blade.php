<!-- The Modal -->
<div class="modal fade" id="comment">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header bg-success">
				<h4 class="modal-title">Comments</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form method="post">
					@csrf
					<div class="form-group">
						<label for="comment">Comment:</label>
						<textarea class="form-control form-control-sm" rows="5"  name="comment" required>{{$prob->comment}}</textarea>
					</div>
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">

			</div>

		</div>
	</div>
</div>