<!-- The Modal -->
<div class="modal fade" id="adddep">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header bg-success">
				<h4 class="modal-title">Add department</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form method="post" action="{{route('postdep')}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
						<label for="email">Department name:</label>
						<input type="text" class="form-control form-control-sm" id="email" name="name" required>
					</div>
					<div class="form-group form-check">

					</div>
					<button type="submit" class="btn btn-outline-success btn-sm">Add</button>
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
			</div>
			</form>
		</div>
	</div>
</div>
<!-- /#page-wrapper -->
