<!-- The Modal -->
<div class="modal fade" id="changeRole">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header bg-success">
				<h4 class="modal-title">Change User type</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form method="post" action="{{route('updaterole')}}">
					@csrf
					<div class="form-group">
						<label for="sel1">Select user type :</label>
						<select class="form-control" id="sel1" name="user_type" required>
							<option value="{{$user->user_type}}">{{$user->user_type}}</option>
							<option value="user">Normal User</option>
							<option value="ict">ICT Staff</option>
							<option value="admin">Admin</option>
						</select>
					</div>
					<div class="form-group">
						<label for="sel1">Select level for only ICT:</label>
						<select class="form-control" id="sel1" name="level">
							<option value="{{$user->level}}">{{$user->level}}</option>
							<option value="Networking">Networking</option>
							<option value="Software">Software</option>
							<option value="Hardware">Hardware</option>
						</select>
					</div>
					<div class="form-group">
						<label for="sel1">Select level for only ICT:</label>
						<select class="form-control" id="sel1" name="title">
							<option value="{{$user->title}}">{{$user->title}}</option>
							<option value="Head">Head</option>
							<option value="Staff">Staff</option>
						</select>
					</div>
						<input type="text" name="id" value="{{$user->id}}" hidden>
					<button type="submit" class="btn btn-outline-success btn-sm">Submit</button>
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">

			</div>

		</div>
	</div>
</div>