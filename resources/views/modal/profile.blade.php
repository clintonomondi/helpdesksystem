<!-- The Modal -->
<div class="modal fade" id="profile">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header bg-success">
				<h4 class="modal-title">Update account</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form method="post" action="{{route('updateaccount')}}">
					@csrf
					<div class="form-group">
						<label for="email">Name:</label>
						<input type="text" class="form-control form-control-sm" id="email" name="name" value="{{$user->name}}" required>
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control form-control-sm" id="password" name="email" value="{{$user->email}}" required>
					</div>
					<div class="form-group">
						<label for="sel1">Department:</label>
						<select class="form-control form-control-sm" id="sel1" name="dep_id" required>
							<option value="{{$user->dep->id}}">{{$user->dep->name}}</option>
							<@foreach($deps as $dep)
							<option value="{{$dep->id}}">{{$dep->name}}</option>
								 @endforeach
						</select>
					</div>

					<div class="form-group form-check">

					</div>
					<button type="submit" class="btn btn-outline-success btn-sm">Submit</button>
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
			</div>
			</form>
		</div>
	</div>
</div>
<!-- /#page-wrapper -->
