<!-- The Modal -->
<div class="modal fade" id="updaterequest">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header bg-success">
				<h4 class="modal-title">Update request</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form method="post" action="{{route('updateprob')}}">
					@csrf
					<div class="form-group">
						<label for="email">Request title:</label>
						<input type="text" class="form-control form-control-sm"  name="title" value="{{$prob->title}}" required>
					</div>
					<div class="form-group">
						<label for="sel1">Category:</label>
						<select class="form-control form-control-sm" id="sel1" name="level" required>
							<option value="{{$prob->level}}">{{$prob->level}}</option>
							<option value="Networking">Networking</option>
							<option value="Software">Software</option>
							<option value="Hardware">Hardware</option>
						</select>
					</div>
					<div class="form-group">
						<label for="comment">Message:</label>
						<textarea class="form-control form-control-sm" rows="5"  name="body" required>{{$prob->body}}</textarea>
						<input type="text" name="id" value="{{$prob->id}}" hidden>
					</div>
					<button type="submit" class="btn btn-outline-success btn-sm">Submit</button>
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
			</div>
			</form>
		</div>
	</div>
</div>
<!-- /#page-wrapper -->
