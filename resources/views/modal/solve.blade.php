<!-- The Modal -->
<div class="modal fade" id="solve">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header bg-success">
				<h4 class="modal-title">Comment</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form method="post" action="{{route('solve')}}">
					@csrf
					<div class="form-group">
						<label for="comment">Comment:</label>
						<textarea class="form-control form-control-sm" rows="5"  name="comment" required></textarea>
					</div>

					<input type="text" name="id" value="{{$prob->id}}" hidden>
					<input type="text" name="status" value="Sorted" hidden>
					<input type="text" name="ict_staff" value="{{Auth::user()->name}}" hidden>
					<button type="submit" class="btn btn-outline-success btn-sm">Forward</button>
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">

			</div>

		</div>
	</div>
</div>