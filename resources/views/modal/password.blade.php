<!-- The Modal -->
<div class="modal fade" id="password">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header bg-success">
				<h4 class="modal-title">Change password</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form method="post" action="{{route('password')}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
						<label for="email">Current password:</label>
						<input type="password" class="form-control form-control-sm" id="email" name="currentpass" required>
					</div>
					<div class="form-group">
						<label for="email">New Password</label>
						<input type="password" class="form-control form-control-sm" id="password" name="newpass">
					</div>
					<div class="form-group">
						<label for="email">Confirm password:</label>
						<input type="password" class="form-control form-control-sm" id="password" name="repass">
					</div>

					<div class="form-group form-check">

					</div>
					<button type="submit" class="btn btn-outline-success btn-sm">Reset</button>
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
			</div>
			</form>
		</div>
	</div>
</div>
<!-- /#page-wrapper -->
