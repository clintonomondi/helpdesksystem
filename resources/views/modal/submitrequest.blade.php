<!-- The Modal -->
<div class="modal fade" id="submitrequest">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header bg-success">
				<h4 class="modal-title">Make request</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form method="post" action="{{route('postproblem')}}">
				@csrf
					<div class="form-group">
						<label for="email">Request title:</label>
						<input type="text" class="form-control form-control-sm"  name="title" required>
					</div>
					<div class="form-group">
						<label for="sel1">Category:</label>
						<select class="form-control form-control-sm" id="sel1" name="level" required>
							<option></option>
							<option value="Networking">Networking</option>
							<option value="Software">Software</option>
							<option value="Hardware">Hardware</option>
						</select>
					</div>
					<div class="form-group">
						<label for="comment">Message:</label>
						<textarea class="form-control form-control-sm" rows="5"  name="body" required></textarea>
					</div>
					<input type="number" name="user_id" value="{{Auth::user()->id}}" hidden>
					<input type="text" name="staff_title" value="{{Auth::user()->title}}" hidden>

					<button type="submit" class="btn btn-outline-success btn-sm">Submit</button>
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<!-- /#page-wrapper -->
