<!-- The Modal -->
<div class="modal fade" id="forward">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header bg-success">
				<h4 class="modal-title">Forward request</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form method="post" action="{{route('forward')}}">
					@csrf
					<div class="form-group">
						<label for="sel1">Select Level :</label>
						<select class="form-control" id="sel1" name="level">
							<option value="{{$prob->level}}">{{$prob->level}}</option>
							<option value="Networking">Networking</option>
							<option value="Software">Software</option>
							<option value="Hardware">Hardware</option>
						</select>
					</div>

					<input type="text" name="id" value="{{$prob->id}}" hidden>
					<button type="submit" class="btn btn-outline-success btn-sm">Forward</button>
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">

			</div>

		</div>
	</div>
</div>