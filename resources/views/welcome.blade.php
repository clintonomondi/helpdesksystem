
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Helpdesk</title>
    <meta name="description" content="Free Bootstrap Theme by uicookies.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,500,700">
    <link rel="stylesheet" href="{{asset('extra/css/styles-merged.css')}}">
    <link rel="stylesheet" href="{{asset('extra/css/style.min.css')}}">

    <!--[if lt IE 9]>
    <script src="{{asset('extra/js/vendor/html5shiv.min.js')}}"></script>
    <script src="{{asset('extra/js/vendor/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body>

<!-- Fixed navbar -->
<!-- navbar-fixed-top  -->


<nav class="navbar probootstrap-megamenu navbar-default probootstrap-navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="" title="uiCookies:Helpdesk">HelpDesk</a>
        </div>

        <div id="navbar-collapse" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                @if (Route::has('login'))
                    <div class="top-right links">
                        @auth
                            <a href="{{ url('/home') }}">Home</a>
                        @else
                            <a href="{{ route('login') }}">Login</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}">Register</a>
                            @endif
                        @endauth
                    </div>
                @endif
            </ul>
        </div>
    </div>
</nav>



<section class="probootstrap-hero">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 text-center probootstrap-hero-text pb0 probootstrap-animate" data-animate-effect="fadeIn">
                <h1>Welcome to ICT help desk!</h1>
                <p>Help us resolve technical issues faster and allow ICT department to work more efficiently.</p>
                <p>
                    <a href="{{route('login')}}" class="btn btn-primary btn-lg" role="button">Login</a>
                    <a href="{{route('register')}}" class="btn btn-primary btn-ghost btn-lg" role="button">Create Account</a>
                </p>
                <!-- <p><a href="#"><i class="icon-play2"></i> Watch the video</a></p> -->
            </div>
        </div>
        </div>
    </div>
</section>
<script src="{{asset('extra/js/scripts.min.js')}}"></script>
<script src="{{asset('extra/js/custom.min.js')}}"></script>
</body>
</html>