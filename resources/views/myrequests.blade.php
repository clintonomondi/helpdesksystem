@extends('layouts.app')
@section('content')
	<div class="container">
		@include('includes.message')
		@include('modal.submitrequest')
		<div class="row justify-content-center">
			<div class="col-sm-12 col-sm-12">
				<div class="feature-block">
					<span class="fa fa-pull-left"><h3>My Requests</h3></span>
					<span class="fa fa-pull-right"><a class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#submitrequest">Submit request</a> </span>
					<table class="table table-striped">
						<thead >
						<th>#</th>
						<th>Level</th>
						<th>Title</th>
						<th>Date</th>
						<th></th>
						<th>Status</th>
						<th></th>
						</thead>
						<tbody id="myTable">
						@if(count($requests)>0)
							@foreach($requests as $request)
								<tr>
									<td>{{$request->id}}</td>
									<td>{{$request->level}}</td>
									<td>{{$request->title}}</td>
									<td>{{$request->created_at}}</td>
									<td>{{$request->created_at->diffForHumans()}}</td>
									@if(($request->status)=='Pending')
										<td><a class="btn btn-primary btn-sm fa fa-sync" href="{{route('readmyreq',$request->id)}}">Pending</a> </td>
									@elseif(($request->status)=='Sorted')
										<td><a class="btn btn-success btn-sm fa fa-check" href="{{route('readmyreq',$request->id)}}" >Solved</a> </td>
									@endif
								</tr>
							@endforeach
						@else
							<p>No  records</p>
						@endif
						</tbody>
					</table>
					{{$requests->links()}}
				</div>
			</div>
		</div>
	</div>
@endsection
