@extends('layouts.app')

@section('content')
<div class="container">
    @include('includes.message')
    @can('isAdmin')
    Dashboard: Admin<hr>
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="card bg-primary" style="color: white;">
                <div class="card-header">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-sync fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">Pending</div>
                            <div>{{$pending}}</div>
                        </div>
                    </div>
                </div>
                <a href="{{route('admin.pending')}}">
                    <div class="card-footer bg-light">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="card bg-success" style="color: white;">
                <div class="card-header">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-check fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">Sorted</div>
                            <div>{{$sorted}}</div>
                        </div>
                    </div>
                </div>
                <a href="{{route('admin.sorted')}}">
                    <div class="card-footer bg-light">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="card bg-info" style="color: white;">
                <div class="card-header">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">Users</div>
                            <div>{{$users}}</div>
                        </div>
                    </div>
                </div>
                <a href="{{route('users')}}">
                    <div class="card-footer bg-light">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
        @endcan

    {{--if not assigned--}}
    @can('isNotAssigned')
            Dashboard: <p style="color:red;">You have not been assigned to the system. Kindly continue inorder to be assigned</p><hr>
            <form method="post" action="{{route('updatedep')}}">
                @csrf
                <div class="form-group">
                    <label for="sel1">Select Department:</label>
                    <select class="form-control form-control-sm" id="sel1" name="dep_id" required>
                        <option></option>
                        @foreach($deps as $dep)
                        <option value="{{$dep->id}}">{{$dep->name}}</option>
                            @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-outline-success btn-sm">Submit</button>
            </form>
        @endcan

    {{--ICT Staff--}}
    @can('isICT')
        Dashboard: ICT Staff<hr>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card bg-primary" style="color: white;">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-sync fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">Pending</div>
                                <div>{{$ictpending}}</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{route('ict.pending')}}">
                        <div class="card-footer bg-light">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card bg-success" style="color: white;">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-check fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">Sorted</div>
                                <div>{{$ictsorted}}</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{route('ict.sorted')}}">
                        <div class="card-footer bg-light">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        @endcan

    {{--User dashboard--}}
    @can('isUser')
        Dashboard: ICT Users<hr>
    Speak with us
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card bg-primary" style="color: white;">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-sync fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">Pending</div>
                                <div>{{$staffpending}}</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{route('myrequests')}}">
                        <div class="card-footer bg-light">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card bg-success" style="color: white;">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-check fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">Sorted</div>
                                <div>{{$staffsorted}}</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{route('myrequests')}}">
                        <div class="card-footer bg-light">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        @endcan
</div>
@endsection
