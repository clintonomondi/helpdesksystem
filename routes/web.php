<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

//admin
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'AdminController@users')->name('users');
Route::get('/departments', 'AdminController@departments')->name('departments');
Route::post('/departments', 'AdminController@postdep')->name('postdep');
Route::get('/departments/edit/{id}', 'AdminController@editdep')->name('editdep');
Route::get('/departments/remove/{id}', 'AdminControlle+r@removedep')->name('removedep');
Route::post('/departments/update', 'AdminController@updatedep')->name('updatedep');
Route::get('/Users/profile/{id}', 'AdminController@userprofile')->name('admin.userprofile');
Route::post('/users/roles', 'AdminController@updaterole')->name('updaterole');
Route::get('/requests/all', 'AdminController@all')->name('admin.all');
Route::get('/requests/pending', 'AdminController@pending')->name('admin.pending');
Route::get('/requests/sorted', 'AdminController@sorted')->name('admin.sorted');
Route::get('/requests/read/{id}', 'AdminController@read')->name('admin.read');
Route::get('/users/remove/{id}', 'AdminController@removeuser')->name('removeuser');


//common
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::post('/profile', 'HomeController@updateprofile')->name('updateprofile');
Route::post('/password', 'HomeController@password')->name('password');
Route::post('/profile/update', 'HomeController@updateaccount')->name('updateaccount');
Route::get('/myrequests', 'HomeController@myrequests')->name('myrequests');
Route::post('/myrequests', 'HomeController@postproblem')->name('postproblem');
Route::post('/myrequests/update', 'HomeController@updateprob')->name('updateprob');
Route::get('/myrequests/view/{id}', 'HomeController@readmyreq')->name('readmyreq');
Route::get('/myrequests/cancel/{id}', 'HomeController@cancel')->name('cancel');
Route::post('/requests/forward', 'HomeController@forward')->name('forward');
Route::post('/requests/solve', 'HomeController@solve')->name('solve');
Route::post('/user/update/dep', 'HomeController@updatedep')->name('updatedep');

Route::get('/ict/pending', 'HomeController@pending')->name('ict.pending');
Route::get('/ict/sorted', 'HomeController@sorted')->name('ict.sorted');

